; nasm -f elf64 hello_x64_stack.asm
; ld -m elf_x86_64 hello_x64_stack.o -o hello_x64_stack

section .text

global _start

_start:
    mov rax, 0x00A21646C726F
    push rax
    mov rax, 0x57202C6F6C6C6548
    push rax
    
    mov rdx, 15
    mov rsi, rsp
    mov rdi, 1
    mov rax, 1
    syscall

    mov rax, 1
    mov rbx, 0
    int 80h
