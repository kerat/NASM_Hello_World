; nasm -f elf64 hello_x64_hardcoded_string.asm
; ld -m elf_x86_64 hello_x64_hardcoded_string.o -o hello_x64_hardcoded_string

section .text

global _start

_start:
   mov rdx, 15
   mov rcx, msg
   mov rbx, 1
   mov rax, 4
   int 80h

   mov rax, 1
   mov rbx, 0
   int 80h

section .data
    msg db 'Hello, world!',13,10
