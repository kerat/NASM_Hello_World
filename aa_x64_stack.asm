; nasm -f elf64 aa_x64_stack.asm
; ld -m elf_x86_64 aa_x64_stack.o -o aa_x64_stack

section .text

global _start

_start:
    push 0x4141
    
    mov rdx, 2
    mov rsi, rsp
    mov rdi, 1
    mov rax, 1
    syscall

    mov rax, 1
    mov rbx, 0
    int 80h
