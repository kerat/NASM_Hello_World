; nasm -f elf32 hello_x32_stack.asm
; ld -m elf_i386 hello_x32_stack.o hello_x32_stack

section .text

global _start

_start:
    push 0x41414141
    
    mov edx, 4
    mov ecx, esp
    mov ebx, 1
    mov eax, 4
    int 80h

    mov eax, 1
    mov ebx, 0
    int 80h
